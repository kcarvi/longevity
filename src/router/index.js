import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

// Importing the home page
import Home from '@/views/Home.vue'

// Create routes
const routes = [
  { path: '/', component: Home }
]

// Connect routes
const router = new Router({
  routes
})

export default router