

## Site structure

Catalogs:

assets - static files (css, image, fonts)

components - components, blocks of code for reusable use

layout - code that is displayed on all pages

router - routing, links to pages

views - site pages


## Installing modules
```
npm install
```

### Server start
```
npm run serve
```

### Compiling the project
```
npm run build
```

